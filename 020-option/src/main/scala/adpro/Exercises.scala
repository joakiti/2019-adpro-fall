// Advanced Programming, A. Wąsowski, IT University of Copenhagen
//
// Group number: 24
//
// AUTHOR1: Mikkel Kaj Andersen
// TIME1: 7 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// AUTHOR2: Rasmus Dilling Møller
// TIME2: 2 hours <- how much time have you used on solving this exercise set
// (excluding reading the book, fetching pizza, and going out for a smoke)
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// This file is compiled with 'sbt compile' and tested with 'sbt test'.
//
// The file shall always compile and run after you are done with each exercise
// (if you do them in order).  Please compile and test frequently. Of course,
// some tests will be failing until you finish. Only hand in a solution that
// compiles and where tests pass for all parts that you finished.    The tests
// will fail for unfnished parts.  Comment such out.

package adpro

// Exercise  1

/* We create OrderedPoint as a trait instead of a class, so we can mix it into
 * Points (this allows to use java.awt.Point constructors without
 * reimplementing them). As constructors are not inherited, We would have to
 * reimplement them in the subclass, if classes not traits are used.  This is
 * not a problem if I mix in a trait construction time. */

trait OrderedPoint extends scala.math.Ordered[java.awt.Point] {

  this: java.awt.Point =>

  override def compare (that: java.awt.Point): Int =  {
    if (this.x < that.x || (this.x == that.x && this.y < that.y)) -1
    else 1
    }

}

// Try the following (and similar) tests in the repl (sbt console):
// val p = new java.awt.Point(0,1) with OrderedPoint
// val q = new java.awt.Point(0,2) with OrderedPoint
// assert(p < q)

// Chapter 3


sealed trait Tree[+A]
case class Leaf[A] (value: A) extends Tree[A]
case class Branch[A] (left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {

  def size[A] (t :Tree[A]): Int = t match {
    case Leaf(value) => 1
    case Branch(left, right) => size(left) + size(right) + 1
  }

  // Exercise 3 (3.26)

  def maximum (t: Tree[Int]): Int = {
    def go(t: Tree[Int]) : Int = t match {
      case Leaf(value) => value
      case Branch(left, right) => go(left).max(go(right))
    }
    go(t)
  }

  // Exercise 4 (3.28)

  def map[A,B] (t: Tree[A]) (f: A => B): Tree[B] = t match {
    case Leaf(value) => Leaf(f(value))
    case Branch(left, right) => Branch(map (left) (f), map (right) (f))
  }

  // Exercise 5 (3.29)

  def fold[A,B] (t: Tree[A]) (f: (B,B) => B) (g: A => B): B = t match {
    case Branch(left, right) => f (fold (left) (f) (g), fold (right) (f) (g))
    case Leaf(value) => g(value)
  }

  def size1[A] (t: Tree[A]): Int = fold (t) ((a:Int, b:Int) => (a + b) + 1) ((c:A) => 1)

  def maximum1[A] (t: Tree[Int]): Int = fold (t) ((a:Int, b: Int) => a max b) ((c:Int) => c)
//Dont know why this last one does not compile
  def map1[A,B] (t: Tree[A]) (f: A=>B): Tree[B] = fold[A,Tree[B]] (t) ((a:Tree[B], b: Tree[B]) => Branch(a,b)) ((c:A) => Leaf(f(c)))
//def map1[A,B] (t: Tree[A]) (f: A=>B): Tree[B] = ???

}

sealed trait Option[+A] {

  // Exercise 6 (4.1)
  def map[B] (f: A=>B): Option[B] = this match {
    case None => None
    case Some(a) => Some(f(a))
  }

  // You may Ignore the arrow in default's type below for the time being.
  // (it should work (almost) as if it was not there)
  // It prevents the argument "default" from being evaluated until it is needed.
  // So it is not evaluated in case of Some (the term is 'call-by-name' and we
  // should talk about this soon).

  def getOrElse[B >: A] (default: => B): B = this match {
    case Some(value) => value
    case None => default
  }

  def flatMap[B] (f: A=>Option[B]): Option[B] = this match {
    case None => None
    case Some(a) => f(a)
  }

  def filter (p: A => Boolean): Option[A] = this match {
    case Some(a) if p(a) => Some(a)
    case _ => None
  }

}

case class Some[+A] (get: A) extends Option[A]
case object None extends Option[Nothing]

object ExercisesOption {

  // Remember that mean is implemented in Chapter 4 of the text book

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  // Exercise 7 (4.2)

  def variance (xs: Seq[Double]): Option[Double] = {
      mean(xs).flatMap(
      m => mean(xs.map(
      x => math.pow(x-m,2))))
    /**
    We tried, but just, too difficult to do for-comprehensions for now. Couldnt quite understand what to do when reaching x.
    for {
      m <- mean(xs)
      x <- mean(xs)
      f <- 
           mean(xs).flatMap(
      m => mean(xs.map(
      x => math.pow(x-m,2))))
      **/
  }

  // Exercise 8 (4.3)

  def map2[A,B,C] (ao: Option[A], bo: Option[B]) (f: (A,B) => C): Option[C] = {
    ao flatMap (a => bo map (b => f(a,b)))
  }
  /**{
    Some(f(ao.getOrElse(), bo.getOrElse()))
  }**/
  
  // Exercise 9 (4.4)
  // We pretty much did not feel we could solve the rest of the exercises.

//  def sequence[A] (aos: List[Option[A]]): Option[List[A]] = aos.flatMap(f => f.foldRight(List()) (_ :: _))
    def sequence[A] (aos: List[Option[A]]): Option[List[A]] = ???
  // Exercise 10 (4.5)

    def traverse[A,B] (as: List[A]) (f :A => Option[B]): Option[List[B]] = ???

}
