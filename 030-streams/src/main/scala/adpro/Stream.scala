// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen
//
// meant to be compiled, for example: fsc Stream.scala
package adpro

sealed trait Stream[+A] {
  import Stream._
 

  def headOption () :Option[A] =
    this match {
      case Empty => None
      case Cons(h,t) => Some(h())
    }

  def tail :Stream[A] = this match {
      case Empty => Empty
      case Cons(h,t) => t()
  }

  def foldRight[B] (z : =>B) (f :(A, =>B) => B) :B = this match {
      case Empty => z
      case Cons (h,t) => f (h(), t().foldRight (z) (f))
      // Note 1. f can return without forcing the tail
      // Note 2. this is not tail recursive (stack-safe) It uses a lot of stack
      // if f requires to go deeply into the stream. So folds sometimes may be
      // less useful than in the strict case
    }

  // Note 1. eager; cannot be used to work with infinite streams. So foldRight
  // is more useful with streams (somewhat opposite to strict lists)
  def foldLeft[B] (z : =>B) (f :(A, =>B) =>B) :B = this match {
      case Empty => z
      case Cons (h,t) => t().foldLeft (f (h(),z)) (f)
      // Note 2. even if f does not force z, foldLeft will continue to recurse
    }

  def exists (p : A => Boolean) :Boolean = this match {
      case Empty => false
      case Cons (h,t) => p(h()) || t().exists (p)
      // Note 1. lazy; tail is never forced if satisfying element found this is
      // because || is non-strict
      // Note 2. this is also tail recursive (because of the special semantics
      // of ||)
    }

  //Exercise 2
  def toList: List[A] = {
    def go(stream: Stream[A], list: List[A]) : List[A] = stream match {
      case Cons(head, tail) => go(tail(), (head() :: list))
      case _ => list.reverse
    }
    go(this, List.empty)
  }

  //Exercise 3
  def take(n: Int): Stream[A] = {
    def go(stream: Stream[A], n: Int) : Stream[A] = 
      if(n==0) {
        Stream.empty
      } else {
        stream match {
          case Cons(head, tail) => cons(head(), go(tail(), n-1))
          case Empty => Stream.empty
        }
      }
    go(this, n)
  }


  def drop(n: Int): Stream[A] = {
    def go(stream: Stream[A], n: Int) : Stream[A] = {
      if(n == 0) {
        stream
      } else {
        stream match {
          case Cons(head, tail) => go(tail(), n-1)
          case Empty => Stream.empty
        }
      }
    }
    go(this, n)
  }
  
  //Exercise 4
  def takeWhile(p: A => Boolean): Stream[A] = ???


  //Exercise 5
  def forAll(p: A => Boolean): Boolean = ???


  //Exercise 6
  def takeWhile2(p: A => Boolean): Stream[A] = ???

  //Exercise 7
  def headOption2 () :Option[A] = ??? 

  //Exercise 8 The types of these functions are omitted as they are a part of the exercises
  def map = ???
  def filter = ???
  def  append = ??? 
  def  flatMap = ???

  //Exercise 09
  //Put your answer here:

  //Exercise 10
  val fibs = {
    def go(fh: Int, ft: Int) : Stream[Int] = {
      cons(fh, go(ft, fh+1));
    }
    go(0,1)
  }


  //Exercise 11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case None => empty;
    case Some((head, tail)) => cons(head, unfold(tail)(f));
  }


  //Exercise 12
  def fib2 = {
    unfold((0,1)) { case (f0,f1) => Some((f0,(f1,f0+f1))) }
  }
  def from2 = unfold(n)(n => Some((n,n+1)))

  //Exercise 13
  def map2[B](f: A => B): Stream[B] =
  unfold(this) {
    case Cons(h,t) => Some((f(h()), t()))
    case _ => None
  }

  def take2(n: Int): Stream[A] =
  unfold((this,n)) {
    case (Cons(h,t), 1) => Some((h(), (empty, 0)))
    case (Cons(h,t), n) if n > 1 => Some((h(), (t(), n-1)))
    case _ => None
  }

  def takeWhile2(f: A => Boolean): Stream[A] =
  unfold(this) {
    case Cons(h,t) if f(h()) => Some((h(), t()))
    case _ => None
  }

  def zipWith2[B,C](s2: Stream[B])(f: (A,B) => C): Stream[C] =
    unfold((this, s2)) {
      case (Cons(h1,t1), Cons(h2,t2)) =>
        Some((f(h1(), h2()), (t1(), t2())))
      case _ => None
  }
}


case object Empty extends Stream[Nothing]
case class Cons[+A](h: ()=>A, t: ()=>Stream[A]) extends Stream[A]

object Stream {

  def empty[A]: Stream[A] = Empty

  def cons[A] (hd: => A, tl: => Stream[A]) :Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def apply[A] (as: A*) :Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
    // Note 1: ":_*" tells Scala to treat a list as multiple params
    // Note 2: pattern matching with :: does not seem to work with Seq, so we
    //         use a generic function API of Seq


  //Exercise 1
  def from(n:Int):Stream[Int]=cons(n,from(n+1))

  def to(n:Int):Stream[Int]= cons(n,from(n-1))

  val naturals: Stream[Int] = from(0)


}

